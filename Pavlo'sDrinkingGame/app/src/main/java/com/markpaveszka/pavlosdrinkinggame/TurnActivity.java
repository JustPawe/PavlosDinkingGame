package com.markpaveszka.pavlosdrinkinggame;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

public class TurnActivity extends AppCompatActivity {


    public TurnActivity() throws IOException {
    }
    private Button startTurnBtn;
    private Button trueBtn;
    private Button falseBtn;
    private Button answerABtn;
    private Button answerBBtn;
    private Button answerCBtn;
    private Button answerDBtn;
    private Button taskCompletedBtn;
    private TextView playerNameTextView;
    private TextView nextTurnIsYoursTextView;
    private TextView trueOrFalseQuestionTextView;
    private TextView multipChoiceQuestionTextView;
    private TextView multipChoiceAnswerATextView;
    private TextView multipChoiceAnswerBTextView;
    private TextView multipChoiceAnswerCTextView;
    private TextView multipChoiceAnswerDTextView;
    private TextView doOrDrinkTextView;
    private TextView doOrDrinkEveryonDrinkTextView;
    private TextView doOrDrinkYouDrinkTextView;
    private ArrayList<String>playerNames = new ArrayList<>();
    private ArrayList<Question>questions = new ArrayList<>();
    private ArrayList<Integer> usedQuestions = new ArrayList<>();
    private BufferedReader reader;
    private int howManyLines=0;
    private Typeface face;

    int generateRange;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_turn);
        face = Typeface.createFromAsset(getAssets(),
                "Squad Font.ttf");

        String lines = null;
        try {
            reader =  new BufferedReader(new InputStreamReader(getAssets().open("questions.txt")));
            lines = reader.readLine();
            while (lines != null) {
                String[] values = lines.split(";");
                int id = Integer.parseInt(values[0]);
                String type = values[1];
                String question = values[2];
                String correctAnswer ;
                String wrongAnswer1 ;
                String wrongAnswer2;
                String wrongAnswer3;

                if (values[3].equals("#NoAnswerHere")){
                    correctAnswer = null;
                }
                else{
                    correctAnswer = values[3];
                }
                if (values[4].equals("#NoAnswerHere")){
                    wrongAnswer1 = null;
                }
                else{
                    wrongAnswer1 = values[4];
                }
                if (values[5].equals("#NoAnswerHere")){
                    wrongAnswer2 = null;
                }
                else{
                    wrongAnswer2 = values[5];
                }
                if (values[6].equals("#NoAnswerHere")){
                    wrongAnswer3 = null;
                }
                else{
                    wrongAnswer3 = values[6];
                }
                Question currentQuestion = new Question(id, type, question, correctAnswer, wrongAnswer1, wrongAnswer2, wrongAnswer3);
                questions.add(currentQuestion);
                lines = reader.readLine();
                howManyLines++;

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        generateRange = questions.size();
        nextTurn();



        //TrueOrFalse:






    }

    private void nextTurn(){
        setContentView(R.layout.activity_turn);
        startTurnBtn =(Button) findViewById(R.id.startTurnBtn);
        playerNameTextView = (TextView) findViewById(R.id.playerNameTextView);
        playerNames = this.getIntent().getStringArrayListExtra("players");
        usedQuestions = this.getIntent().getIntegerArrayListExtra("usedQuestions");
        nextTurnIsYoursTextView =(TextView) findViewById(R.id.nexTurnIsYoursTextView);
        nextTurnIsYoursTextView.setTypeface(face);

        Random random = new Random();
        int playerNumber = random.nextInt(playerNames.size());
        String currentPlayer = playerNames.get(playerNumber);
        playerNameTextView.setText(currentPlayer);

        Random random1 = new Random();
        int questionNumber=0;
        while (questionNumber ==0){
            questionNumber = random1.nextInt(generateRange);
        }
        final Question placerVariable = questions.get(questionNumber);
        /*questions.remove(questionNumber);
        questions.add(placerVariable);
        generateRange--;
        if (generateRange==1){
            generateRange=questions.size();
        }*/

        startTurnBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (placerVariable.returnQuestionType().equals("TrueOrFalse")){
                    setContentView(R.layout.true_or_false_layout);
                    trueOrFalse(placerVariable);

                }
                if (placerVariable.returnQuestionType().equals("MultipChoice")){
                    setContentView(R.layout.multiple_choice_layout);
                    multipleChoice(placerVariable);
                }
                if (placerVariable.returnQuestionType().equals("DrinkOrDoIt")){
                    setContentView(R.layout.do_or_drink_layout);
                    doOrDrink(placerVariable);
                }


            }
        });
    }

    private void trueOrFalse(final Question question){


        trueOrFalseQuestionTextView = (TextView)findViewById(R.id.questionTFDisplayTextView);
        String currentQuestion = question.returnQuestionContent();
        trueOrFalseQuestionTextView.setTypeface(face);
        trueOrFalseQuestionTextView.setText(currentQuestion);
        trueBtn = (Button) findViewById(R.id.trueBtn);
        falseBtn = (Button)findViewById(R.id.falseBtn);



        trueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (question.returnCorrectAnswer().equals("True")){
                    ConstraintLayout constraintLayout = (ConstraintLayout)findViewById(R.id.trueorfalselayout);
                    constraintLayout.setBackgroundColor(Color.parseColor("#09ef1c"));
                    Random random = new Random();
                    int howManyShots =0;
                    while (howManyShots==0){
                        howManyShots= random.nextInt(7);
                    }
                    new AlertDialog.Builder(TurnActivity.this)
                            .setMessage("Correct! Everyone else, drink " + howManyShots +" times" )
                            .setPositiveButton("Go for the next turn", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    nextTurn();
                                }
                            })
                            .show();
                }
                if(!question.returnCorrectAnswer().equals("True")) {
                    ConstraintLayout constraintLayout = (ConstraintLayout)findViewById(R.id.trueorfalselayout);
                    constraintLayout.setBackgroundColor(Color.parseColor("#f41616"));
                    Random random = new Random();
                    int howManyShots =0;
                    while (howManyShots==0){
                        howManyShots= random.nextInt(7);
                    }

                    new AlertDialog.Builder(TurnActivity.this)
                            .setMessage("Wrong! Drink " + howManyShots+" times")
                            .setPositiveButton("Go for the next turn", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    nextTurn();
                                }
                            })
                            .show();
                }
            }
        });
        falseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (question.returnCorrectAnswer().equals("False")){
                    ConstraintLayout constraintLayout = (ConstraintLayout)findViewById(R.id.trueorfalselayout);
                    constraintLayout.setBackgroundColor(Color.parseColor("#09ef1c"));
                    Random random = new Random();
                    int howManyShots =0;
                    while (howManyShots==0){
                        howManyShots= random.nextInt(7);
                    }
                    new AlertDialog.Builder(TurnActivity.this)
                            .setMessage("Correct! Everyone else, drink " + howManyShots +" times")
                            .setPositiveButton("Go for the next turn", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    nextTurn();
                                }
                            })
                            .show();
                }
                if(!question.returnCorrectAnswer().equals("False")){
                    ConstraintLayout constraintLayout = (ConstraintLayout)findViewById(R.id.trueorfalselayout);
                    constraintLayout.setBackgroundColor(Color.parseColor("#f41616"));
                    Random random = new Random();
                    int howManyShots =0;
                    while (howManyShots==0){
                        howManyShots= random.nextInt(7);
                    }
                    new AlertDialog.Builder(TurnActivity.this)
                            .setMessage("Wrong! Drink " + howManyShots+" times")
                            .setPositiveButton("Go for the next turn", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    nextTurn();
                                }
                            })
                            .show();

                }
            }
        });




    }


    private void multipleChoice(final Question question){
        multipChoiceQuestionTextView = (TextView) findViewById(R.id.questionMCDisplayTextView);
        multipChoiceQuestionTextView.setTypeface(face);
        multipChoiceAnswerATextView =(TextView) findViewById(R.id.answerATextView);
        multipChoiceAnswerATextView.setTypeface(face);
        multipChoiceAnswerBTextView = (TextView) findViewById(R.id.answerBTextView);
        multipChoiceAnswerBTextView.setTypeface(face);
        multipChoiceAnswerCTextView = (TextView) findViewById(R.id.answerCTextView);
        multipChoiceAnswerCTextView.setTypeface(face);
        multipChoiceAnswerDTextView = (TextView) findViewById(R.id.answerDTextView);
        multipChoiceAnswerDTextView.setTypeface(face);
        String currentQuestion = question.returnQuestionContent();

        multipChoiceQuestionTextView.setText(currentQuestion);
        String correctAnswerOfCurrentQuestion = question.returnCorrectAnswer();
        String wrongAnswer1 = question.returnWrongAnswer1();
        String wrongAnswer2 = question.returnWrongAnswer2();
        String wrongAnswer3 = question.returnWrongAnswer3();
        Random random2 = new Random();
        final int placeOfTheCorrectAnswer = random2.nextInt(4);
        // 0-A, 1-B, 2-C, 3-B
        switch (placeOfTheCorrectAnswer){
            case 0:
                multipChoiceAnswerATextView.setText("A: "+correctAnswerOfCurrentQuestion);
                break;
            case 1:
                multipChoiceAnswerBTextView.setText("B: "+correctAnswerOfCurrentQuestion);
                break;
            case 2:
                multipChoiceAnswerCTextView.setText("C: "+correctAnswerOfCurrentQuestion);
                break;
            case 3:
                multipChoiceAnswerDTextView.setText("D: "+correctAnswerOfCurrentQuestion);
                break;
        }

        int placeOfWrongAnswer1 = (placeOfTheCorrectAnswer+1)%4;
        int placeOfWrongAnswer2;
        if(placeOfWrongAnswer1==3){
            placeOfWrongAnswer2 = 0;
        }
        else{
            placeOfWrongAnswer2 = placeOfWrongAnswer1+1;

        }

        int placeOfWrongAnswer3 = (placeOfTheCorrectAnswer+3)%4;
        switch (placeOfWrongAnswer1){
            case 0:
                multipChoiceAnswerATextView.setText("A: "+wrongAnswer1);
                break;
            case 1:
                multipChoiceAnswerBTextView.setText("B: "+wrongAnswer1);
                break;
            case 2:
                multipChoiceAnswerCTextView.setText("C: "+wrongAnswer1);
                break;
            case 3:
                multipChoiceAnswerDTextView.setText("D: "+wrongAnswer1);
                break;
        }

        switch (placeOfWrongAnswer2){
            case 0:
                multipChoiceAnswerATextView.setText("A: "+wrongAnswer2);
                break;
            case 1:
                multipChoiceAnswerBTextView.setText("B: "+wrongAnswer2);
                break;
            case 2:
                multipChoiceAnswerCTextView.setText("C: "+wrongAnswer2);
                break;
            case 3:
                multipChoiceAnswerDTextView.setText("D: "+wrongAnswer2);
                break;
        }

        switch (placeOfWrongAnswer3){
            case 0:
                multipChoiceAnswerATextView.setText("A: " +wrongAnswer3);
                break;
            case 1:
                multipChoiceAnswerBTextView.setText("B: "+wrongAnswer3);
                break;
            case 2:
                multipChoiceAnswerCTextView.setText("C: "+wrongAnswer3);
                break;
            case 3:
                multipChoiceAnswerDTextView.setText("D: "+wrongAnswer3);
                break;
        }

        answerABtn = (Button) findViewById(R.id.answer1Btn);
        answerBBtn = (Button) findViewById(R.id.answer2Btn);
        answerCBtn = (Button) findViewById(R.id.answer3Btn);
        answerDBtn = (Button) findViewById(R.id.answer4Btn);

        answerABtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             if(placeOfTheCorrectAnswer==0){
                 ConstraintLayout constraintLayout = (ConstraintLayout)findViewById(R.id.multiplechoicelayout);
                 constraintLayout.setBackgroundColor(Color.parseColor("#09ef1c"));
                 Random random = new Random();
                 int howManyShots =0;
                 while (howManyShots==0){
                     howManyShots= random.nextInt(7);
                 }
                 new AlertDialog.Builder(TurnActivity.this)
                         .setMessage("Correct! Everyone else, drink " + howManyShots +" times")
                         .setPositiveButton("Go for the next turn", new DialogInterface.OnClickListener() {
                             @Override
                             public void onClick(DialogInterface dialog, int which) {
                                 nextTurn();
                             }
                         })
                         .show();
             }

             else{
                 ConstraintLayout constraintLayout = (ConstraintLayout)findViewById(R.id.multiplechoicelayout);
                 constraintLayout.setBackgroundColor(Color.parseColor("#f41616"));
                 Random random = new Random();
                 int howManyShots =0;
                 while (howManyShots==0){
                     howManyShots= random.nextInt(7);
                 }
                 new AlertDialog.Builder(TurnActivity.this)
                         .setMessage("Wrong! Drink " + howManyShots+" times")
                         .setPositiveButton("Go for the next turn", new DialogInterface.OnClickListener() {
                             @Override
                             public void onClick(DialogInterface dialog, int which) {
                                 nextTurn();
                             }
                         })
                         .show();
             }
            }
        });
        answerBBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(placeOfTheCorrectAnswer==1){
                    ConstraintLayout constraintLayout = (ConstraintLayout)findViewById(R.id.multiplechoicelayout);
                    constraintLayout.setBackgroundColor(Color.parseColor("#09ef1c"));
                    Random random = new Random();
                    int howManyShots =0;
                    while (howManyShots==0){
                        howManyShots= random.nextInt(7);
                    }
                    new AlertDialog.Builder(TurnActivity.this)
                            .setMessage("Correct! Everyone else, drink " + howManyShots +" times")
                            .setPositiveButton("Go for the next turn", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    nextTurn();
                                }
                            })
                            .show();
                }

                else{
                    ConstraintLayout constraintLayout = (ConstraintLayout)findViewById(R.id.multiplechoicelayout);
                    constraintLayout.setBackgroundColor(Color.parseColor("#f41616"));
                    Random random = new Random();
                    int howManyShots =0;
                    while (howManyShots==0){
                        howManyShots= random.nextInt(7);
                    }
                    new AlertDialog.Builder(TurnActivity.this)
                            .setMessage("Wrong! Drink " + howManyShots+" times")
                            .setPositiveButton("Go for the next turn", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    nextTurn();
                                }
                            })
                            .show();
                }
            }
        });
        answerCBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(placeOfTheCorrectAnswer==2){
                    ConstraintLayout constraintLayout = (ConstraintLayout)findViewById(R.id.multiplechoicelayout);
                    constraintLayout.setBackgroundColor(Color.parseColor("#09ef1c"));
                    Random random = new Random();
                    int howManyShots =0;
                    while (howManyShots==0){
                        howManyShots= random.nextInt(7);
                    }
                    new AlertDialog.Builder(TurnActivity.this)
                            .setMessage("Correct! Everyone else, drink " + howManyShots +" times")
                            .setPositiveButton("Go for the next turn", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    nextTurn();
                                }
                            })
                            .show();
                }

                else{
                    ConstraintLayout constraintLayout = (ConstraintLayout)findViewById(R.id.multiplechoicelayout);
                    constraintLayout.setBackgroundColor(Color.parseColor("#f41616"));
                    Random random = new Random();
                    int howManyShots =0;
                    while (howManyShots==0){
                        howManyShots= random.nextInt(7);
                    }
                    new AlertDialog.Builder(TurnActivity.this)
                            .setMessage("Wrong! Drink " + howManyShots+" times")
                            .setPositiveButton("Go for the next turn", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    nextTurn();
                                }
                            })
                            .show();
                }
            }
        });

        answerDBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(placeOfTheCorrectAnswer==3){
                    ConstraintLayout constraintLayout = (ConstraintLayout)findViewById(R.id.multiplechoicelayout);
                    constraintLayout.setBackgroundColor(Color.parseColor("#09ef1c"));
                    Random random = new Random();
                    int howManyShots =0;
                    while (howManyShots==0){
                        howManyShots= random.nextInt(7);
                    }
                    new AlertDialog.Builder(TurnActivity.this)
                            .setMessage("Correct! Everyone else, drink " + howManyShots +" times")
                            .setPositiveButton("Go for the next turn", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    nextTurn();
                                }
                            })
                            .show();
                }

                else{
                    ConstraintLayout constraintLayout = (ConstraintLayout)findViewById(R.id.multiplechoicelayout);
                    constraintLayout.setBackgroundColor(Color.parseColor("#f41616"));
                    Random random = new Random();
                    int howManyShots =0;
                    while (howManyShots==0){
                        howManyShots= random.nextInt(7);
                    }
                    new AlertDialog.Builder(TurnActivity.this)
                            .setMessage("Wrong! Drink " + howManyShots+" times")
                            .setPositiveButton("Go for the next turn", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    nextTurn();
                                }
                            })
                            .show();
                }
            }
        });

    }

    private void doOrDrink(final Question question){
        doOrDrinkTextView =(TextView) findViewById(R.id.questionDDDisplayTextView);
        doOrDrinkTextView.setTypeface(face);
        String currentQuestion = question.returnQuestionContent();
        doOrDrinkTextView.setText(currentQuestion);
        doOrDrinkEveryonDrinkTextView =(TextView) findViewById(R.id.everyoneDrinkTextView);
        doOrDrinkEveryonDrinkTextView.setTypeface(face);
        doOrDrinkYouDrinkTextView =(TextView) findViewById(R.id.youDrinkTextView);
        doOrDrinkYouDrinkTextView.setTypeface(face);
        Random random= new Random();
        Random random1 = new Random();
        int howManyShotsForEveryone =0;
        while (howManyShotsForEveryone==0){
            howManyShotsForEveryone= random.nextInt(7);
        }
        int howManyShotsForYou =0;
        while (howManyShotsForYou==0){
            howManyShotsForYou= random.nextInt(7);
        }
        doOrDrinkEveryonDrinkTextView.setText("Everyone drinks "+howManyShotsForEveryone+ " if you do it");
        doOrDrinkYouDrinkTextView.setText("You drink "+howManyShotsForYou+" if you fail or you pass");
        taskCompletedBtn = (Button) findViewById(R.id.taskCompletedBtn);

        taskCompletedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(TurnActivity.this)
                        .setMessage("If you did the task well done, else please drink!")
                        .setPositiveButton("Go for the next turn", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                nextTurn();
                            }
                        })
                        .show();
            }
        });


    }
}
